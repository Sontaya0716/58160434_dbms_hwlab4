CREATE VIEW empdept AS
	SELECT	e.Fname, e.Lname, e.Salary,
		d.Dname,
		dl.LocationName
	FROM
		Employee e
			INNER JOIN
		Department d ON d.Dnumber = e.Dno
			INNER JOIN
		Dept_Locations dl ON dl.Lnumber = d.Location_id
	GROUP BY e.EmpID
	ORDER BY e.Salary ASC;
