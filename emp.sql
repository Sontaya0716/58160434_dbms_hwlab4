CREATE VIEW emp AS
        SELECT  e.Fname, e.Lname, e.Salary,
                d.Dname
        FROM
                Employee e
                        INNER JOIN
                Department d ON d.Dnumber = e.Dno
        GROUP BY e.EmpID
        ORDER BY e.Salary ASC;
