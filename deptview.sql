CREATE VIEW dept AS
	SELECT d.Dname, dl.LocationName AS LocationName
	FROM Department AS d
	LEFT JOIN ( Dept_Locations AS dl )
	ON ( d.Location_id = dl.Lnumber )
	GROUP BY d.Dnumber
	ORDER BY Mgr_start_date ASC;
