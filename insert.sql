INSERT INTO Dept_Locations VALUES(NULL, 'Rayong');
INSERT INTO Dept_Locations VALUES(NULL, 'Srakoew');
INSERT INTO Dept_Locations VALUES(NULL, 'Bangsaen');
INSERT INTO Dept_Locations VALUES(NULL, 'Samudpraharn');

INSERT INTO Manager VALUES(NULL, 'Mr.John', 'Lenon', 'MNG-100', 'M', '1990-01-10', 'USA');
INSERT INTO Manager VALUES(NULL, 'Ms.Victoria', 'Benditt', 'MNG-200', 'M', '1989-04-12', 'UK');
INSERT INTO Manager VALUES(NULL, 'Mr.Hemmarat', 'Wachirahatthapong', 'MNG-300', 'F', '1995-02-21', 'Thailand');
INSERT INTO Manager VALUES(NULL, 'Mr.Chanlee', 'Kuwong', 'MNG-400', 'M', '1990-01-10', 'China');

INSERT INTO Department VALUES(NULL, 'Sale Engineer', '1', '2015-07-20', '1');
INSERT INTO Department VALUES(NULL, 'Marketing', '2', '2016-03-14', '2');
INSERT INTO Department VALUES(NULL, 'IT', '3', '2015-01-01', '3');
INSERT INTO Department VALUES(NULL, 'Production', '4', '2016-12-01', '4');

INSERT INTO Employee VALUES(NULL, 'Mr.Wittawas', 'Teptong', 'EMP-100', '1990-01-16', 'Thailand', 'M', '21000.00', '1');
INSERT INTO Employee VALUES(NULL, 'Mr.Decha', 'Thonghan', 'EMP-200', '1990-06-16', 'Thailand', 'M', '22300.00', '2');
INSERT INTO Employee VALUES(NULL, 'Ms.Nutcha', 'Wongpudee', 'EMP-300', '1990-09-16', 'Thailand', 'F', '27500.00', '3');
INSERT INTO Employee VALUES(NULL, 'Ms.Dawdee', 'Rungrueng', 'EMP-400', '1990-12-16', 'Thailand', 'F', '37500.00', '4');
