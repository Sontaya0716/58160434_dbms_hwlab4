CREATE VIEW management AS
	SELECT	d.Dnumber, d.Dname,
		m.Fname, m.Lname
	FROM Department AS d
	LEFT JOIN ( Manager AS m )
	ON ( d.ManagerID = m.MNumber )
	GROUP BY d.Dnumber
        ORDER BY Mgr_start_date ASC;
